﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bloackLook : MonoBehaviour {

    public float speed;
     Vector3 startpos;

    void Start()
    {
        startpos = transform.position;
    }
    void Update()
    {
        transform.Translate((new Vector3(-1, 0 , 0)) * speed * Time.deltaTime);
        if (transform.position.x < -174)
        {
            transform.position = startpos;
        }
    }
}

