﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudsLook : MonoBehaviour {

    public float speed;
    Vector3 startpos;
	// Use this for initialization

	void Start () {
        startpos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {

        transform.Translate((new Vector3(-1, 0 , 0)) * speed * Time.deltaTime);

        if(transform.position.x < -147)
        {
            transform.position = startpos;
        }
	}
}
